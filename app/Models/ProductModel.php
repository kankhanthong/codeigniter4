<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'user_id';

    protected $allowedFields = [
        'username',
        'password',
        'role',
        'name',
        'email',
        'phone_number'
    ];
}
