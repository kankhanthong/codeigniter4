<!DOCTYPE html>
<html lang="en">

<head>
    <?= $template['head']; ?>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-dark pe-3 ps-3">
        <?= $template['menu']; ?>
    </nav>
    <main>
        <?= $template['content']; ?>
    </main>
    <footer class="bg-dark">
        <?= $template['footer']; ?>
    </footer>
</body>

</html>