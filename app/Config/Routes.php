<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/home', 'Home::index');
$routes->get('/product', 'Product::index');
$routes->get('/about/shipping', 'About::Shipping');
$routes->get('/about/contact', 'About::Contact');


//crud
$routes->get('/user', 'User::index');
$routes->get('/user/create', 'User::Create');
$routes->post('/user/create/submit', 'User::SubmitCreate');
$routes->get('/user/update/(:num)', 'User::Update/$1');
$routes->post('/user/update/submit', 'User::SubmitUpdate');
$routes->get('/user/delete/(:num)', 'User::Delete/$1');

//auth
$routes->get('/login', 'Login::index');
$routes->post('/login/check', 'Login::Check');
$routes->get('/logout', 'logout::index');
