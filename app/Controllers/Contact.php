<?php

namespace App\Controllers;

use App\Controllers\Template;

class Contact extends BaseController
{
    public function index(): string
    {
        $template = new Template();
        return $template->Render('Contact/Index',);
    }
}
