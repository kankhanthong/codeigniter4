<?php 
namespace App\Controllers;

class Logout extends BaseController{
    public function Index(){
        session()->destroy();
        return redirect()->to('/home');
    }
}