<?php

namespace App\Controllers;

use App\Models\UserModel;

class Template extends BaseController
{
    public function Render($view, $data = []): string
    {
        if(session()->get('logged_in')){
            $rowUser = (new UserModel())->find(session()->get('user_id'));
            $data['loggedUser'] = $rowUser;
        }
        $data['template'] = array(
            'head' => view('template/Head', $data),
            'menu' => view('template/Menu', $data),
            'footer' => view('template/Footer', $data),
            'content' => view($view, $data)
        );
        return view('Template/layout', $data);
    }
}
