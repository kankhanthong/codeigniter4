<?php

namespace App\Controllers;

use App\Controllers\Template;

class Product extends BaseController
{
    public function index(): string
    {
        $template = new Template();
        return $template->Render('Product/Index');
    }
}
