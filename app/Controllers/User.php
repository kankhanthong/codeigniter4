<?php

namespace App\Controllers;

use App\Controllers\Template;
use App\Models\UserModel;

class User extends BaseController
{
    public function index()
    {
        if (!session()->get('logged_in')) {
            return redirect()->to('/login');
        }
        
        $userModel = new UserModel();
        $users = $userModel->orderBy('name', 'asc')->findAll();

        return (new Template())->render('User/Index', [
            'title' => 'รายชื่อผู้ใช้งาน',
            'users' => $users
        ]);
    }

    public function create()
    {
        if (!session()->get('logged_in')) {
            return redirect()->to('/login');
        }

        return (new Template())->render('User/Create', [
            'title' => 'เพิ่มผู้ใช้งาน'
        ]);
    }

    public function submitCreate()
    {
        if (!session()->get('logged_in')) {
            return redirect()->to('/login');
        }

        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        $name = $this->request->getPost('name');
        $email = $this->request->getPost('email');
        $phoneNumber = $this->request->getPost('phoneNumber');

        $errors = [];
        if (empty($username)) {
            $errors[] = 'ชื่อผู้ใช้งาน';
        }
        if (empty($password)) {
            $errors[] = 'รหัสผ่าน';
        }
        if (empty($name)) {
            $errors[] = 'ชื่อ-นามสกุล';
        }

        if (!empty($errors)) {
            return (new Template())->render('User/SubmitCreate', [
                'title' => 'เพิ่มผู้ใช้งาน',
                'error' => true,
                'message' => 'กรุณากรอกข้อมูล ' . join(', ', $errors) . ' ให้ครบถ้วน'
            ]);
        }

        $userModel = new UserModel();
        if ($userModel->where('username', $username)->countAllResults() > 0) {
            return (new Template())->render('User/SubmitCreate', [
                'title' => 'เพิ่มผู้ใช้งาน',
                'error' => true,
                'message' => 'มีผู้ใช้ชื่อ ' . $username . ' อยู่ในระบบแล้ว'
            ]);
        }

        $insert = $userModel->insert([
            'username' => $username,
            'password' => $password,
            'name' => $name,
            'email' => $email,
            'phone_number' => $phoneNumber
        ]);

        if ($insert) {
            return (new Template())->render('User/SubmitCreate', [
                'title' => 'เพิ่มผู้ใช้งาน',
                'error' => false,
                'message' => 'เพิ่มผู้ใช้งานเรียบร้อยแล้ว'
            ]);
        }

        return (new Template())->render('User/SubmitCreate', [
            'title' => 'เพิ่มผู้ใช้งาน',
            'error' => true,
            'message' => 'เกิดข้อผิดพลาดในการเพิ่มผู้ใช้งาน'
        ]);
    }

    public function update($id)
    {
        if (!session()->get('logged_in')) {
            return redirect()->to('/login');
        }
        if (empty($id)) {
            return redirect()->to('/user');
        }
        
        $userModel = new UserModel();
        $rowUser = $userModel->find($id);
        if (empty($rowUser)) {
            return redirect()->to('/user');
        }
        
        return (new Template())->render('user/update', [
            'title' => 'แก้ไขผู้ใช้งาน',
            'rowUser' => $rowUser
        ]);
    }

    public function submitUpdate()
    {
        if (!session()->get('logged_in')) {
            return redirect()->to('/login');
        }
        
        $id = $this->request->getPost('id');
        $username = $this->request->getPost('username');
        $name = $this->request->getPost('name');
        $email = $this->request->getPost('email');
        $phoneNumber = $this->request->getPost('phoneNumber');
        
        $userModel = new UserModel();
        $rowUser = $userModel->find($id);
        
        if (empty($rowUser)) {
            return (new Template())->render('User/SubmitUpdate', [
                'title' => 'แก้ไขผู้ใช้งาน',
                'error' => true,
                'message' => 'ไม่พบผู้ใช้งานที่ต้องการแก้ไข',
                'id' => $id
            ]);
        }
        
        $errors = [];
        if (empty($username)) {
            $errors[] = 'ชื่อผู้ใช้งาน';
        }
        if (empty($name)) {
            $errors[] = 'ชื่อ นามสกุล';
        }
        
        if (!empty($errors)) {
            return (new Template())->render('User/SubmitUpdate', [
                'title' => 'แก้ไขผู้ใช้งาน',
                'error' => true,
                'message' => 'กรอกข้อมูล ' . join(', ', $errors) . ' ให้ครบถ้วน',
                'id' => $id
            ]);
        }
        
        if ($userModel->where([
            'username' => $username,
            'user_id !=' => $id
        ])->countAllResults() > 0) {
            return (new Template())->render('User/SubmitUpdate', [
                'title' => 'แก้ไขผู้ใช้งาน',
                'error' => true,
                'message' => 'ชื่อผู้ใช้ ' . $username . ' อยู่ในระบบแล้ว',
                'id' => $id
            ]);
        }
        
        $update = $userModel->update($id, [
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'phoneNumber' => $phoneNumber
        ]);
        
        if ($update) {
            return (new Template())->render('User/SubmitUpdate', [
                'title' => 'แก้ไขผู้ใช้งาน',
                'error' => false,
                'message' => 'แก้ไขผู้ใช้งานเรียบร้อยแล้ว'
            ]);
        }
        
        return (new Template())->render('User/SubmitUpdate', [
            'title' => 'แก้ไขผู้ใช้งาน',
            'error' => true,
            'message' => 'แก้ไขผู้ใช้งานไม่สำเร็จ',
            'id' => $id
        ]);
    }

    public function Delete($id)
{
    if (!session()->get('logged_in')) {
        return redirect()->to('/login');
    }

    $userModel = new UserModel();
    $rowUser = $userModel->find($id);

    if (empty($rowUser)) {
        return (new Template)->Render('User/Delete', [
            'title' => 'augtuonu',
            'error' => true,
            'message' => 'wly'
        ]);
    }

    $delete = $userModel->delete($id);

    if ($delete) {
        return (new Template)->Render('User/Delete', [
            'title' => 'augtuɔnu',
            'error' => false,
            'message' => 'ลบผู้ใช้งานเรียบร้อยแล้ว'
        ]);
    } else {
        return (new Template)->Render('User/Delete', [
            'title' => 'augtuɔnu',
            'error' => true,
            'message' => 'augTzonuluianiša'
        ]);
    }
}
}
