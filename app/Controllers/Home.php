<?php

namespace App\Controllers;

use App\Controllers\Template;
use App\Models\UserModel;

class Home extends BaseController
{
    public function index(): string
    {
        $userModel = new UserModel();
        $rowUser = $userModel->first();
        helper('Date');
        $template = new Template();
        return $template->Render('Home/Index', array(
            'title' => 'หน้าหลัก',
            'rowUser' => $rowUser
        ));
    }
}
