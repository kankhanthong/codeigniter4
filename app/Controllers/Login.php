<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Controllers\Template;

class Login extends BaseController
{
    public function index()
    {
        if (session()->get('logged_in')) {
            return redirect()->to('/');
        }

        return (new Template())->render('login/index', [
            'title' => 'เข้าสู่ระบบ'
        ]);
    }

    public function check()
    {
        if (session()->get('logged_in')) {
            return redirect()->to('/');
        }

        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');

        $template = new Template();

        if (empty($username) || empty($password)) {
            return $template->render('login/check', [
                'title' => 'ตรวจสอบการใช้งานของระบบ',
                'error' => true,
                'message' => 'กรุณากรอกข้อมูลให้ครบถ้วน'
            ]);
        }

        $userModel = new UserModel();
        $rowUser = $userModel->where('username', $username)
                            ->where('password', $password)
                            ->first();

        if (empty($rowUser)) {
            return $template->render('login/check', [
                'title' => 'ตรวจสอบการใช้งานของระบบ',
                'error' => true,
                'message' => 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง'
            ]);
        }

        session()->set('logged_in', true);
        session()->set('user_id', $rowUser['user_id']);

        return $template->render('login/check', [
            'title' => 'ล็อกอินสำเร็จ',
            'error' => false,
            'message' => 'สวัสดี ' . $rowUser['name']
        ]);
    }
}
